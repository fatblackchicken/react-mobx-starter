import React, { Component } from 'react';
import {observer} from 'mobx-react';
import DevTools from 'mobx-react-devtools';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

import logo from './logo.svg';
import './App.css';

@observer
class App extends Component {
  componentDidMount() {
    let store = this.props.store;
    setInterval(() => {
      ++store.counter;
    }, 1000);
  }
  
  render() {
    let store = this.props.store;
    return (
      <MuiThemeProvider>
        <Card className="App">
          <CardMedia className="App-header" 
            overlay={<CardTitle title="Welcome to React" />}>
            <img src={logo} className="App-logo" alt="logo" />
          </CardMedia>
          <CardText>
            To get started, edit <code>src/App.js</code> and save to reload.
          </CardText>
          <CardText>
            Counter: {store.counter}
          </CardText>
          <DevTools />
        </Card>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
	store: React.PropTypes.object.isRequired
};

export default App;
