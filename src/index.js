import React from 'react';
import {render} from 'react-dom';
import App from 'components/App';
import AppStore from 'stores/AppStore';
import './index.css';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();
let appStore = new AppStore();

console.log(appStore);

render(
  <App store={appStore}/>,
  document.getElementById('root')
);
