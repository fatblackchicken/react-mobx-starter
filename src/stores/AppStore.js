import {observable} from 'mobx';
import {observableLocalStorage} from 'utils/localStorage.mobx';

export default class AppStore {
	@observableLocalStorage("hello") counter;
}
