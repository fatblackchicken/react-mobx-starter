export function LocalStorage(storageKey, options) {
  let { cache } = options || {};
  let value = cache ? localStorage.getItem(storageKey) : null; // value has no use in non-cache mode
  // this decorator doesn't explicitly handle errors like non-configurable properties
  // and cache mode may return not up-to-date value if the same localStorage item is modified elsewhere.
  return function(target, key, descriptor) {
    let { configurable, enumerable, writable} = descriptor;
    console.log(descriptor);
    if (!writable) {
      let message = '@LocalStorage decorator must be applied to a writable property';
      console.log(message);
      throw { message };
    }
    
    let getCached = function() { return value; }
    let getDirect = function() { return localStorage.getItem(storageKey); }
    
    return {
      configurable,
      enumerable,
      set: function(vv) {
        value = vv;
        if (vv == null) localStorage.removeItem(storageKey); // neither JSON nor localStorage support `undefined` value
        else localStorage.setItem(storageKey, JSON.stringify(vv));
        console.log('value ' + storageKey + ' set to ' + vv);
      },
      get: (cache ? getCached : getDirect)
    };
  };
}
