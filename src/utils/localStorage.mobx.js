import { observable } from 'mobx';

export function observableLocalStorage(storageKey) {
  return function(target, key, descriptor) {
    let mobxDescriptor = observable(target, key, descriptor);
    let { get, set } = mobxDescriptor;
    let newDescriptor = mobxDescriptor;
    
    let setStorage = function(value) {
      console.log(`Setting localStorage: ${storageKey} = ${value}`);
      if (value == null) localStorage.removeItem(storageKey); // neither JSON nor localStorage support `undefined` value
      else localStorage.setItem(storageKey, JSON.stringify(value));
    }
    
    let init = function(instance) {
      init.hasRun = true;
      let descriptor = Object.getOwnPropertyDescriptor(instance, key);
      let { get, set } = descriptor;
      if (!(get && set)) return;
      
      descriptor.set = function(v1) {
        setStorage(v1);
        return set.call(this, v1);
      }
      
      descriptor.get = function() {
        let value = get.call(this);
        let localValue = localStorage.getItem(storageKey);
        if (value !== localValue) {
          console.log(`WARNING: localStorage.getItem('${storageKey}') is different from cached value`);
        }
        return localValue;
      }
      
      Object.defineProperty(instance, key, descriptor);
    }
    
    // this define getter & setter at object's __proto__ level. 
    // But then mobx add another getter/setter pair at object level when the first get/set is called,
    // effectively nullify the following getter and setter. We have to intercept the new getter/setter again, in function init()
    newDescriptor.get = function() {
      //console.log('newDescriptor.get');
      let result = get.call(this);
      //console.log('result=', result);
      if (init.hasRun) return result;
      init(this);
      
      if (result == null) { //  the variable is declared without an assignment
        let localValue = localStorage.getItem(storageKey);
        console.log('returning ', localValue);
        return localValue;        
      }
      
      //  else: set the value
        
      setStorage(result);
      return result;
    }
    
    newDescriptor.set = function(v0) {
      //console.log('newDescriptor.set', v0);
      let result = set.call(this, v0);
      if (init.hasRun) return result;
      init(this);
      setStorage(v0);
      return result;
    }
    
    return newDescriptor;
  }
}